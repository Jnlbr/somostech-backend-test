# Aplicacion realizada para el proceso de prueba de Somos Tech

Antes de iniciar la aplicacion debe actualizar las credenciales de base de datos en el archivo .env 

Unas vez descargado el repositorio, debe instalar las dependencias con el comando
    
    yarn install OR npm install

Para iniciar la aplicacion utilice el comando

    yarn start OR npm start

Las tablas se cargaran automaticamente a la base de datos. 


## Peticiones
Se podran realizar las siguientes peticiones al servidor, por medio de la url http://localhost:10036

1. GET: obtener una lista de usuarios, con sus respectivos datos basicos y adicionales

2. POST: crear un usuario con los datos basicos y adicionales del mismo


### Ejemplos

#### GET

Realizar la peticion de tipo GET a *http://localhost:10036*
  
#### POST

Realizar la peticion de tipo POST a *http://localhost:10036* con los siguientes datos:

1. Campos obligatorios: 
      1. email: String
      2. fullname: String
      3. lastname: String
      4. address: String
      5. phoneNumber: String
2. Campos opcionales: 
      1. music: Array[Object]
      2. art: Array[Object]
      3. movie: Array[Object]

Cada objeto debe contener unicamente el campo name, por ejemplo:

    {
      "name":"I bet my life"
    }

Ejemplo del cuerpo de una peticion: 

    {
      "email":"tyrion@gmail.com",
      "fullname":"Tyrion",
      "lastname":"Lannister",
      "address":"Kings landing",
      "phoneNumber":"+1 11111111111",
      "music": [{
        "name": "Dragons",
      }, {
        "name": "I see fire"
      }],
      "art": [{
        "name": "El purgatorio"
      }]
    }