const express = require('express');
const models = require('./models').models;

const { User, Art, Movie, Music } = models;

const router = express.Router();

router.get('/', async (req,res) => {
  const send = (status,body) => res.status(status).send({status,body});
  const data = await User.findAll({
    include: [{
      model: Music,
    }, {
      model: Movie
    }, {
      model: Art
    }]
  });
  // console.log(await data[12].getMusic())
  send(200, { message: 'OK', data });
});

router.post('/', async (req,res) => {
  const send = (status,body) => res.status(status).send({status,body});
  const { email, fullname, lastname, address, phoneNumber, music, movie, art } = req.body;
  try {
    const exist = await User.findByEmail(email);
    if(exist) {
      send(200, {message:'La direccion de correo ya esta en uso'});
      return;
    }
    const user = await User.create({email, fullname, lastname, address, phoneNumber });
    const userMusic = await addMusic(user.id, music);
    const userMovie = await addMovie(user.id, movie);
    const userArt = await addArt(user.id, art);
    send(200, { message: 'OK', body: {
      user,
      music: userMusic,
      movie: userMovie,
      art: userArt
    }});
  } catch(err) {
    send(500, { message: err.message || err });
  }
});

const addMusic = async (userId,music) => {
  return await addData(userId,Music,music);
}
const addArt = async (userId,art) => {
  return await addData(userId,Art,art);
}
const addMovie = async (userId,movie) => {
  return await addData(userId,Movie,movie);
}

const addData = async (userId, Model, data) => {
  if(data !== null && data !== undefined) {
    const promises = data.map(async d => {
      return await Model.create({userId, ...d});
    });

    return await Promise.all(promises);
  }

  return null;
}

module.exports = router;