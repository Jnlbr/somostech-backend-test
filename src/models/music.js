const { Model } = require('sequelize');

class Music extends Model {
  // Relations
  static associate(models) {
    this.belongsTo(models.User);
  }
};

function music(sequelize, dataTypes) {
  Music.init({
    name: {
      type: dataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2,32]
      }
    }
  }, {
    sequelize,
    modelName: 'music',
    tableName: 'music',
    underscored: true
  });

  return Music;
}

module.exports.Music = Music;
module.exports = music;