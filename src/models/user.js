const { Model } = require('sequelize');

class User extends Model {
  // Relations
  static associate(models) {
    this.hasMany(models.Art);
    this.hasMany(models.Movie);
    this.hasMany(models.Music);
  }

  static async findByEmail(email) {
    let user = await this.findOne({
      where: { email },
    });
  
    return user;
  }

}

function user(sequelize, dataTypes) {
  
  User.init({
    email: {
      type: dataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true,
        isEmail: true,
        len:[9,128]
      },
    },
    fullname: {
      type: dataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [2, 64]
      }
    },
    lastname: {
      type: dataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [2, 64]
      }
    },
    address: {
      type: dataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2,128]
      }
    },
    phoneNumber: {
      type: dataTypes.STRING,
      allowNull: false,
      validate: {
        len: [5,32]
      }
    }
  }, {
    sequelize,
    modelName: 'user',
    tableName: 'user',
    underscored: true
  });

  // Hooks
  User.beforeCreate(async user => {
    
  });


  return User;
}

module.exports.User = User;
module.exports = user;