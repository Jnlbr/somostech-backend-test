const { Model } = require('sequelize');

class Movie extends Model {
  // Relations
  static associate(models) {
    this.belongsTo(models.User);
  }
};

function movie(sequelize, dataTypes) {
  Movie.init({
    name: {
      type: dataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2,32]
      }
    }
  }, {
    sequelize,
    modelName: 'movie',
    tableName: 'movie',
    underscored: true
  });

  return Movie;
}

module.exports.Movie = Movie;
module.exports = movie;