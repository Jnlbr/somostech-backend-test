const { Model } = require('sequelize');

class Art extends Model {
  // Relations
  static associate(models) {
    this.belongsTo(models.User);
  }
};

function art(sequelize, dataTypes) {
  Art.init({
    name: {
      type: dataTypes.STRING,
      allowNull: false,
      validate: {
        len: [2,32]
      }
    }
  }, {
    sequelize,
    modelName: 'art',
    tableName: 'art',
    underscored: true
  });

  return Art;
}

module.exports.Art = Art;
module.exports = art;