require('dotenv/config');
const cors = require('cors');
// const morgan = require('morgan');
const http = require('http');
const express = require('express');
const { models, sequelize } =  require('./models');

const app = express();

app.use(cors());
app.use(express.json())

// app.use(morgan('dev'));

app.use('/', require('./endpoints'))

const httpServer = http.createServer(app);

const isTest = !!process.env.TEST_DATABASE;
const isProduction = !!process.env.DATABASE_URL;
const port = process.env.PORT || 8000;

sequelize.sync({ force: isTest || isProduction }).then(async () => {
  // if (isTest || isProduction) {
    // Mock data goes here
  // }

  httpServer.listen({ port }, () => {
    console.log(`Server listening on http://localhost:${port}`);
  });
});